**Download**  [sonar client](https://docs.sonarqube.org/display/SCAN/Analyzing+with+SonarQube+Scanner)

Run **docker-compose up -d**

Go to **localhost:9000**

Login system (login: **admin**: password: **admin**)

Follow Sonar's instructions

Copy and change **scan.sh.dist** in Symfony bundle(**src**) directory

**Run the scan.sh and rejoice in life**

scan.sh example:
sonar-scanner -Dsonar.projectKey=dev-symfony -Dsonar.sources=. -Dsonar.host.url=http://localhost:9000 -Dsonar.login=3bb94d91e711e2a83e106a81e9dced0595ff6d03